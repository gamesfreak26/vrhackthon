﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelFireInteraction : MonoBehaviour
{

    public FuelType fuel;
    public bool goodOrBadFuel;
    public int fuelValue;

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "fire")
        {
            if (FireBrennt.instance.initDone)
            {
                FireBrennt.instance.OnFuelAdd(fuel, goodOrBadFuel, fuelValue);
            }
            Destroy(this.gameObject as GameObject);
        }
    }


}
