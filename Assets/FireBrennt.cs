﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.Animations;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.PostProcessing;
public enum FuelType
{
    wood, plastic, glass, metal, ice, paper, water, other
}
public class FireBrennt : MonoBehaviour
{
    public bool initDone = false;
    private float previousFireLevel;
   [SerializeField] private float FireLevel;
    public float StartingFireLevel = 90;
    public float fireDecRatePerSec = 1;//1 is -1 FireLevel per second
    public float[] fireDecRatePerDifficulty = new float[4];
    public float[] fireThreshold = new float[4];
    public float[] diffTimeThreshold = new float[4];
    public float[] heartBeatIncreasingPitch = new float[3];
    public float[] heartBeatIncreasingVolume = new float[3];
    private bool gameIsOver = false;
    private bool playonce1 = true;
    private bool playonce2 = true;
    private bool playonce3 = true;
    private bool playonce4 = true;
    private bool playonce5 = true;
    [Header("Scene object")]
    public Animation fireSparks;

    [Header("UI")]
    [SerializeField] TextMeshProUGUI tutorialText;
    public GameObject AURA;
    public PostProcessingProfile newPP;
    public Transform blinkingEyeParent;

    [Header("Animation")]
    [SerializeField] Animation textFadeInOut;

    [Header("Resources")]
    [SerializeField] AudioClip[] AllAudioClips;

    [Header("Audio")]
    public GameObject bgmAudio;
    public GameObject sfxAudio;
    public GameObject monsterAudio;
    public GameObject inFireAudio;
    public GameObject pickupAudio;
    public GameObject fireLevelAudio;
    public GameObject gameStateAudio;
    public GameObject tutorialAudio;

    //audio
    AudioSource bgmAudioSource;
    AudioSource heartBeatAudioSource;
    AudioSource sfxAudioSource;
    AudioSource monsterAudioSource;
    AudioSource inFireAudioSource;
    AudioSource pickUpAudioSource;
    AudioSource fireLevelChangeAudioSource;
    AudioSource gameStateAudioSource;
    AudioSource tutorialAudioSource;



    private void Start()
    {
        
        sfxAudioSource = this.gameObject.AddComponent<AudioSource>();
       // newPP.vignette.settings newVig = newPP.vignette.settings;
    }
    private List<GameObject> getAllBlinkingEyeGO()
    {
        List<GameObject> newList = new List<GameObject>();
        for (int i = 0; i < 11; i++)
        {
            newList.Add(blinkingEyeParent.GetChild(i).gameObject);
        }
        return newList;
    }
    private void RandomlySelectBlinkEyes(int stage)
    {
        List<GameObject> newList = new List<GameObject>();
        newList = getAllBlinkingEyeGO();
        foreach(GameObject go in newList)
        {
            go.SetActive(false);
        }
        int count = 0;
        switch (stage)
        {
            case 1:
                while(count < 3)
                {
                    int randInt = Random.Range(0, newList.Count);
                    newList[randInt].SetActive(true);
                    newList.Remove(newList[randInt]);
                    count++;
                }
                break;
            case 2:
                while (count < 6)
                {
                    int randInt = Random.Range(0, newList.Count);
                    newList[randInt].SetActive(true);
                    newList.Remove(newList[randInt]);
                    count++;
                }
                break; 
            case 3:
                while (count < 11)
                {
                    int randInt = Random.Range(0, newList.Count);
                    newList[randInt].SetActive(true);
                    newList.Remove(newList[randInt]);
                    count++;
                }
                break;
        }
    }
    /// <summary>
    /// changes game state between initilizing beginning values and resetting values when game ends, called at start of game and an end condition
    /// </summary>
    /// <param name="trueBeginFalseEnd">true will initilize for starting the game and false will initlize for the end of the game</param>
    private void InitGameState(bool trueBeginFalseEnd)
    {
        if (trueBeginFalseEnd)
        {
            InitSounds(false);
            FireLevel = StartingFireLevel;
            StartCoroutine(WaitForDiffInc());//increase difficulty overtime
            StartCoroutine(FireDecreaseOverTime());//decrease fireLevel overTime
        }
    }


    /// <summary>
    /// adds as many audiosources as there are audioclips and then add the sources to a list of audiosources that is index wise same as the audioClip list
    /// </summary>
    private void InitSounds(bool trueFlat)
    {
        if (trueFlat)
        {
            bgmAudioSource = this.gameObject.AddComponent<AudioSource>();
            heartBeatAudioSource = this.gameObject.AddComponent<AudioSource>();
            sfxAudioSource = this.gameObject.AddComponent<AudioSource>();
            monsterAudioSource = this.gameObject.AddComponent<AudioSource>();
            inFireAudioSource = this.gameObject.AddComponent<AudioSource>();
            pickUpAudioSource = this.gameObject.AddComponent<AudioSource>();
            fireLevelChangeAudioSource = this.gameObject.AddComponent<AudioSource>();
            gameStateAudioSource = this.gameObject.AddComponent<AudioSource>();
            tutorialAudioSource = this.gameObject.AddComponent<AudioSource>();
        }
        else
        {
            bgmAudioSource = bgmAudio.AddComponent<AudioSource>();
            heartBeatAudioSource = this.gameObject.AddComponent<AudioSource>();
            sfxAudioSource = sfxAudio.gameObject.AddComponent<AudioSource>();
            monsterAudioSource = monsterAudio.gameObject.AddComponent<AudioSource>();
            inFireAudioSource = inFireAudio.gameObject.AddComponent<AudioSource>();
            pickUpAudioSource = pickupAudio.gameObject.AddComponent<AudioSource>();
            fireLevelChangeAudioSource = fireLevelAudio.gameObject.AddComponent<AudioSource>();
            gameStateAudioSource = gameStateAudio.gameObject.AddComponent<AudioSource>();
            tutorialAudioSource = tutorialAudio.gameObject.AddComponent<AudioSource>();
        }

    }
 

    /// <summary>
    /// an ieunumerator that is called within couroutine on start init, it will wait a set number of seconds before increasing the game's difficulty
    /// </summary>
    /// <returns></returns>
    IEnumerator WaitForDiffInc()
    {
        yield return new WaitForSeconds(diffTimeThreshold[0]);
        fireDecRatePerSec = fireDecRatePerDifficulty[0];
        PlayTriggerDiffUpSound();
        if (playonce3)
        {
            RandomlySelectBlinkEyes(1);
            tutorialText.text = "the fire burns quicker by the minute . . .";
            if (tutorialText.GetComponent<Animation>().isPlaying)
            {
                tutorialText.GetComponent<Animation>().Stop();
            }
            tutorialText.GetComponent<Animation>().Play();
            playonce3 = false;
        }
        Debug.Log("diff increased");
        yield return new WaitForSeconds(diffTimeThreshold[1]);
        RandomlySelectBlinkEyes(2);
        tutorialText.text = "the fire burns quicker by the minute . . .";
        if (tutorialText.GetComponent<Animation>().isPlaying)
        {
            tutorialText.GetComponent<Animation>().Stop();
        }
        tutorialText.GetComponent<Animation>().Play();
        fireDecRatePerSec = fireDecRatePerDifficulty[1];
        PlayTriggerDiffUpSound();
        tutorialText.text = "the fire burns quicker by the minute . . .";
        if (tutorialText.GetComponent<Animation>().isPlaying)
        {
            tutorialText.GetComponent<Animation>().Stop();
        }
        tutorialText.GetComponent<Animation>().Play();
        Debug.Log("diff increased");
        RandomlySelectBlinkEyes(3);
        yield return new WaitForSeconds(diffTimeThreshold[2]);
        fireDecRatePerSec = fireDecRatePerDifficulty[2];
        PlayTriggerDiffUpSound();
        tutorialText.text = "the fire burns quicker by the minute . . .";
        if (tutorialText.GetComponent<Animation>().isPlaying)
        {
            tutorialText.GetComponent<Animation>().Stop();
        }
        tutorialText.GetComponent<Animation>().Play();
        Debug.Log("diff increased");
        yield return new WaitForSeconds(diffTimeThreshold[3]);
        bgmAudioSource.Stop();
        bgmAudioSource.clip = AllAudioClips[8];
        bgmAudioSource.volume = 0.4f;
        bgmAudioSource.Play();
        StartCoroutine(WinGame());

    }

    IEnumerator WinGame()
    {
        tutorialText.text = "dawn is near, you live to see another day!";
        if (tutorialText.GetComponent<Animation>().isPlaying)
        {
            tutorialText.GetComponent<Animation>().Stop();
        }
        tutorialText.GetComponent<Animation>().Play();
        AURA.SetActive(true);
        yield return new WaitForSeconds(6);
        ResetGame(false);
    }
    /// <summary>
    /// an ienumerator that is ran every second while the game is not over, it will wait a second pefore calling for the deduction of FireLevel
    /// </summary>
    /// <returns></returns>
    IEnumerator FireDecreaseOverTime()
    {
        while (!gameIsOver&&playonce5)
        {
            FloatRememberPreviousFireLevel(FireLevel);
            Debug.Log("current fire is :" + FireLevel);
            Debug.Log("current fire threhold lv is  :" + CheckFireLevelThreshold(FireLevel));
            yield return new WaitForSeconds(1);
            FireLevel -= fireDecRatePerSec;
            CompareFireLevelBeforeAfter();
            if (FireLevel < fireThreshold[0])
            {
                PlayGameStateSFX(false);
                playonce5 = false;
            }
        }
    }


    /// <summary>
    /// used to return the fireLevel representation via checking with the fireLevel threshold
    /// </summary>
    /// <param name="fire">the instance of fireLevel that is being checked</param>
    /// <returns></returns>
    private int CheckFireLevelThreshold(float fire)
    {
        if (fire > fireThreshold[2])
        {
            return 3;
        }
        else if(fire > fireThreshold[1])
        {
            return 2;
        }
        else if(fire > fireThreshold[0])
        {
            return 1;
        }
        return 0;
    }


    /// <summary>
    /// calls when fuel is added, saves changes to fireLevels, compare to decide on levelUp or Down and call correct triggerables
    /// </summary>
    /// <param name="goodOrBad">set true if fuel added is good</param>
    public void OnFuelAdd(FuelType fuelAdded, bool goodOrBad, int value)
    {
        fireSparks.Play();
        FloatRememberPreviousFireLevel(FireLevel);
        if (goodOrBad)
        {
            if (playonce2)
            {
                tutorialText.text = "Good fuel feeds the fire . . .";
                if (tutorialText.GetComponent<Animation>().isPlaying)
                {
                    tutorialText.GetComponent<Animation>().Stop();
                }
                tutorialText.GetComponent<Animation>().Play();
                playonce2 = false;
            }
            FireLevel += value;
        }
        else
        {
            if (playonce1)
            {
                tutorialText.text = "Not all things are good for burning . . .";
                if (tutorialText.GetComponent<Animation>().isPlaying)
                {
                    tutorialText.GetComponent<Animation>().Stop();
                }
                tutorialText.GetComponent<Animation>().Play();
                playonce1 = false;
            }

            FireLevel -= value;
        }
        CompareFireLevelBeforeAfter(fuelAdded, true, goodOrBad);
    }


    /// <summary>
    /// checks if the addition of fuel caused a change in fireLevel, if so determine the change was up or down and call the correct triggerables, if no change to fire level then 
    /// activate triggerables based on whether the fuel was good or bad
    /// </summary>
    /// <param name="fuelAddition"> whether this is a possible change in fireLevel caused by fuel addition or not</param>
    /// <param name="goodFuel">whether the fuel is good or bad is checked outside, and if good then pass this as true</param>
    private void CompareFireLevelBeforeAfter(FuelType fuelAdded = FuelType.other, bool fuelAddition = false, bool goodFuel = false)
    {
        if(CheckFireLevelThreshold(previousFireLevel) > CheckFireLevelThreshold(FireLevel))
        {
            FireLevelDownGrade();
        }
        else if(CheckFireLevelThreshold(previousFireLevel) < CheckFireLevelThreshold(FireLevel))
        {
            FireLevelUpGrade();
        }
        else
        {
            if (fuelAddition)
            {
                FuelAddTriggers(goodFuel, fuelAdded);
            }
            else
            {
                //play basic fire light animation that occurs per second 
            }
        }
    }


    /// <summary>
    /// this is called before changing the FireLevel via fuel, it is used to check if the change was at different thresholds or the same
    /// </summary>
    /// <param name="fire"></param>
    private void FloatRememberPreviousFireLevel(float fire)
    {
        previousFireLevel = FireLevel;
    }
    

    /// <summary>
    /// everything that is triggered when fireLevel decreases a level
    /// </summary>
    private void FireLevelDownGrade()
    {
        PlayUpgradeSFX(false);
        playFireUpgradeAnim(false);
    }


    /// <summary>
    /// everything that is triggered when fireLevel increases a level
    /// </summary>
    private void FireLevelUpGrade()
    {
        PlayUpgradeSFX(true);
        playFireUpgradeAnim(true);
    }


    /// <summary>
    /// activate everything that is triggered on fuel add, determined by whether the fuel added was good or bad
    /// </summary>
    private void FuelAddTriggers(bool goodOrBadFuel, FuelType fuelAdded)
    {
        if(CheckFireLevelThreshold(FireLevel) == 3)
        {
            playFireUpgradeAnim(true);
        }
        PlayInFireSfx(fuelAdded);
    }


    #region sounds
    private void PlayBGM(int bgmTrack, bool loop = true,  float volume=-1, float pitch=-1)
    {
        if (bgmAudioSource.isPlaying)
        {
            bgmAudioSource.Stop();
        }
        switch (bgmTrack)
        {
            case 0:
                bgmAudioSource.clip = AllAudioClips[1];
                break;
            case 1:
                bgmAudioSource.clip = AllAudioClips[2];
                break;
            case 2:
                bgmAudioSource.clip = AllAudioClips[3];
                break;
            case 3:
                bgmAudioSource.clip = AllAudioClips[4];
                break;
            case 4:
                bgmAudioSource.clip = AllAudioClips[5];
                break;
        }
        if(volume >0)
        {
            bgmAudioSource.volume = volume;
        }
        if(pitch > 0)
        {
            bgmAudioSource.pitch = pitch;
        }
        if (loop)
        {
            bgmAudioSource.loop = true;
        }
        bgmAudioSource.Play();
    }
    private void heartBeatAudio(bool loop = true, float volume = -1, float pitch =-1)
    {
        if (initDone) {
            if (heartBeatAudioSource.isPlaying) {
                heartBeatAudioSource.Stop();
            }
            heartBeatAudioSource.loop = true;
            heartBeatAudioSource.clip = AllAudioClips[0];
            if (volume > 0) {
                heartBeatAudioSource.volume = volume;
            }
            if (pitch > 0) {
                heartBeatAudioSource.pitch = pitch;
            }
            heartBeatAudioSource.Play();
        }
    }
    private void PlayHeartBeatSpeed(int whichHeartBeatSpeed)
    {
        switch (whichHeartBeatSpeed)
        {
            case 0:
                heartBeatAudio(true, heartBeatIncreasingVolume[0], heartBeatIncreasingPitch[0]);
                break;
            case 1:
                heartBeatAudio(true, heartBeatIncreasingVolume[1], heartBeatIncreasingPitch[1]);
                break;
            case 2:
                heartBeatAudio(true, heartBeatIncreasingVolume[2], heartBeatIncreasingPitch[2]);
                break;
        }
    }
    private void PlayInFireSfx(FuelType fuel, float volume =-1, float speed = -1)
    {
        if (inFireAudioSource.isPlaying)
        {
            inFireAudioSource.Stop();
        }
        switch (fuel)
        {
            case FuelType.glass:
                inFireAudioSource.clip = AllAudioClips[12];
                break;
            case FuelType.ice:
                inFireAudioSource.clip = AllAudioClips[13];
                break;
            case FuelType.metal:
                inFireAudioSource.clip = AllAudioClips[14];
                break;
            case FuelType.other:
                inFireAudioSource.clip = AllAudioClips[15];
                break;
            case FuelType.paper:
                inFireAudioSource.clip = AllAudioClips[16];
                break;
            case FuelType.plastic:
                inFireAudioSource.clip = AllAudioClips[17];
                break;
            case FuelType.water:
                inFireAudioSource.clip = AllAudioClips[18];
                break;
            case FuelType.wood:
                inFireAudioSource.clip = AllAudioClips[19];
                break;
        }
        if(volume> 0)
        {
            inFireAudioSource.volume = volume;
        }
        if (speed > 0)
        {
            inFireAudioSource.pitch = speed;
        }
        inFireAudioSource.Play();
    }
    public void PlayPickUpSFX(FuelType fuel, float volume =-1, float speed = -1)
    {
        if (pickUpAudioSource.isPlaying)
        {
            pickUpAudioSource.Stop();
        }
        switch (fuel)
        {
            case FuelType.glass:
                pickUpAudioSource.clip = AllAudioClips[25];
                break;
            case FuelType.ice:
                pickUpAudioSource.clip = AllAudioClips[25];
                break;
            case FuelType.metal:
                pickUpAudioSource.clip = AllAudioClips[23];
                break;
            case FuelType.other:
                pickUpAudioSource.clip = AllAudioClips[25];
                break;
            case FuelType.paper:
                pickUpAudioSource.clip = AllAudioClips[21];
                break;
            case FuelType.plastic:
                pickUpAudioSource.clip = AllAudioClips[22];
                break;
            case FuelType.water:
                pickUpAudioSource.clip = AllAudioClips[25];
                break;
            case FuelType.wood:
                pickUpAudioSource.clip = AllAudioClips[26];
                break;
        }
        if (volume > 0)
        {
            pickUpAudioSource.volume = volume;
        }
        if (speed > 0)
        {
            pickUpAudioSource.pitch = speed;
        }
        pickUpAudioSource.Play();
    }
    private void PlayMonsterSfx(int triggerTrack, float volume = -1, float speed = -1)
    {
        if (monsterAudioSource.isPlaying)
        {
            monsterAudioSource.Stop();
        }
        switch (triggerTrack)
        {
            case 0:
                monsterAudioSource.clip = AllAudioClips[6];
                break;
            case 1:
                monsterAudioSource.clip = AllAudioClips[7];
                break;
            case 2:
                monsterAudioSource.clip = AllAudioClips[10];
                break;
        }
        if (volume > 0)
        {
            monsterAudioSource.volume = volume;
        }
        if (speed > 0)
        {
            monsterAudioSource.pitch = speed;
        }
        monsterAudioSource.Play();
    }
    private void PlayTriggerDiffUpSound()
    {
        if (monsterAudioSource.isPlaying)
        {
            monsterAudioSource.Stop();
        }
        monsterAudioSource.clip = AllAudioClips[9];
        monsterAudioSource.Play();
    }
    private void PlayUpgradeSFX(bool upgradeOrDownGrade, float volume = -1, float speed = -1)
    {
        if (fireLevelChangeAudioSource.isPlaying)
        {
            fireLevelChangeAudioSource.Stop();
        }
        if (upgradeOrDownGrade)
        {
            fireLevelChangeAudioSource.clip = AllAudioClips[33];
        }
        else
        {
            fireLevelChangeAudioSource.clip = AllAudioClips[31];
        }
        if (volume > 0)
        {
            fireLevelChangeAudioSource.volume = volume;
        }
        if (speed > 0)
        {
            fireLevelChangeAudioSource.pitch = speed;
        }
        fireLevelChangeAudioSource.Play();
    }
    private void PlayGameStateSFX(bool beginOrEnd, float volume = -1, float pitch = -1)
    {
        if (beginOrEnd)
        {
            StartCoroutine(playStartSound());
        }
        else
        {
            initDone = false;
            StartCoroutine(playEndAudio());
        }
    }
    IEnumerator playStartSound()
    {
        InitSounds(false);
        int newRnadBgmIndex = Random.Range(0, 5);
        PlayBGM(newRnadBgmIndex, true, 0.5f);
        yield return new WaitForSeconds(1);
        gameStateAudioSource.clip = AllAudioClips[34];//monster voice 3 seconds
        gameStateAudioSource.volume = 0.7f;
        gameStateAudioSource.Play();
        yield return new WaitForSeconds(1);
        sfxAudioSource.clip = AllAudioClips[28];//bg crescendo 6 seconds
        sfxAudioSource.volume = 0.6f;
        sfxAudioSource.Play();
        yield return new WaitForSeconds(2);
        gameStateAudioSource.clip = AllAudioClips[29];//light up fire 3 seconds
        gameStateAudioSource.Play();
        yield return new WaitForSeconds(2);
        sfxAudioSource.clip = AllAudioClips[30];
        sfxAudioSource.Play();
        yield return new WaitForSeconds(1);
        PlayUpgradeSFX(true);//play upgrade sound 1 //fire whoosh
        InitGameState(true);
        initDone = true;
        tutorialText.GetComponent<Animation>().Play();
        playFireUpgradeAnim(true);
        yield return new WaitForSeconds(1);
        PlayTriggerDiffUpSound();
    }
    IEnumerator playEndAudio()
    {
        bgmAudioSource.Stop();
        sfxAudioSource.Stop();
        monsterAudioSource.Stop();
        inFireAudioSource.Stop();
        PlayMonsterSfx(2);
        sfxAudioSource.clip = sfxAudioSource.clip = AllAudioClips[28];
        sfxAudioSource.Play();
        yield return new WaitForSeconds(4);
        gameStateAudioSource.clip = AllAudioClips[34];
        gameStateAudioSource.Play();
        if (playonce4)
        {
            tutorialText.text = "The darkness consumes you . . . ";
            tutorialText.GetComponent<Animation>().Play();
            playonce4 = false;
        }
        yield return new WaitForSeconds(3);
        ResetGame(false);
    }
    private void ResetGame(bool firstSetGame)
    {
        if (firstSetGame)
        {
            PlayGameStateSFX(true);
        }
        else
        {
            SceneManager.LoadScene(0);
        }
    }

    private void PlayTutorialSfx()
    {
        if (sfxAudioSource.isPlaying)
        {
            sfxAudioSource.Stop();
        }
        sfxAudioSource.clip = AllAudioClips[27];
        sfxAudioSource.Play();
    }
    #endregion


    private bool playHBonce = true;
    private bool playHBonce1 = true;
    private bool playHBonce2 = true;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ResetGame(true);
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            ResetGame(false);
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            PlayUpgradeSFX(true);
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            PlayUpgradeSFX(false);
        }
        if (Input.GetKeyDown(KeyCode.V))
        {
            PlayPickUpSFX(FuelType.other);
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            PlayInFireSfx(FuelType.other);
        }
        if (playHBonce && FireLevel< 65)
        {
            PlayHeartBeatSpeed(0);
        }
        if (playHBonce1 && FireLevel < 45)
        {
            PlayHeartBeatSpeed(1);
        }
        if (playHBonce2 && FireLevel < 25)

        {
            PlayHeartBeatSpeed(2);
        }
    }


    #region animation
    public Animator fireAnim;
    //true 1 : 01
    //true 2 : 12
    //true 3 : 23
    //false 1 : 10

    private void playFireUpgradeAnim(bool isUpgrade)
    {
        fireAnim.ResetTrigger("u");
        fireAnim.ResetTrigger("d");
        if (isUpgrade)
        {
                fireAnim.SetTrigger("u");
        }
        else
        {
                fireAnim.SetTrigger("d");
        }
    }

    #endregion

    



    public static FireBrennt instance = null;
    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }
}
