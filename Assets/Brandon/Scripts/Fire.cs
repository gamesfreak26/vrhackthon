﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    public Animator fireAnim;

    // Start is called before the first frame update
    void Start()
    {
        //fireAnim.SetTrigger("01");
    }

    public void playFireUpgradeAnim(bool isUpgrade, int upgradeAnimIndex)
    {
        if (isUpgrade)
        {
            if (upgradeAnimIndex == 1)
            {
                fireAnim.SetTrigger("01");
            }
            else if (upgradeAnimIndex == 2)
            {
                fireAnim.SetTrigger("12");
            }
            else
            {
                fireAnim.SetTrigger("23");
            }

        }

        if (isUpgrade == false)
        {
            if (upgradeAnimIndex == 1)
            {
                fireAnim.SetTrigger("10");
            }
            else if (upgradeAnimIndex == 2)
            {
                fireAnim.SetTrigger("21");
            }
            else
            {
                fireAnim.SetTrigger("32");
            }

        }
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("q"))
        {
            playFireUpgradeAnim(true, 1);
        }

        else if (Input.GetKeyDown("w")) {
            playFireUpgradeAnim(true, 2);
        }

        else if (Input.GetKeyDown("e"))
        {
            playFireUpgradeAnim(true, 3);
        }

        else if (Input.GetKeyDown("a"))
        {
            playFireUpgradeAnim(false, 1);
        }
        else if (Input.GetKeyDown("s"))
        {
            playFireUpgradeAnim(false, 2);
        }
        else if (Input.GetKeyDown("d"))
        {
            playFireUpgradeAnim(false, 3);
        }
    }
}
